# Fast Quadtree
[![pipeline](https://gitlab.com/findley/fast-quadtree/badges/master/pipeline.svg)](https://gitlab.com/findley/fast-quadtree/commits/master)
[![coverage](https://gitlab.com/findley/fast-quadtree/badges/master/coverage.svg)](https://gitlab.com/findley/fast-quadtree/commits/master)
[![npm](https://img.shields.io/npm/v/fast-quadtree.svg)](https://www.npmjs.com/package/fast-quadtree)

A fast and efficient quadtree implemented in typescript.

## Installation

**Fast Quadtree** can be installed with npm or yarn.

```bash
npm install fast-quadtree
# or
yarn add fast-quadtree
```

## Usage
### Setup
```typescript
const quadtree = new Quadtree<string>(new Box(0, 0, 100, 100));
```

### Insert
```typescript
quadtree.insert('item 1', new Point(3, 4));
quadtree.insert('item 2', new Point(5, 6));
```

### Remove
```typescript
quadtree.remove('item 2', 3, 4);
```

### Query
```typescript
const itemsInCircle = quadtree.findInShape(new Circle(10, 10, 20));
const itemsInBox = quadtree.findInShape(new Box(5, 5, 10, 10));
const itemsOnPoint = quadtree.findInShape(new Point(3, 4));
```

### Update
todo

### Cleanup
todo

## Changelog

The changelog is available [here](https://gitlab.com/findley/fast-quadtree/blob/master/CHANGELOG.md).

## License

This library is under the [MIT License](https://gitlab.com/findley/fast-quadtree/blob/master/LICENSE).
