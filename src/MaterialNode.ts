import { Box } from './shapes/Box';

export class MaterialNode {
    public rect: Box;
    public nodeIndex: number;
    public depth: number;
    
    constructor(rect: Box, index: number = 0, depth: number = 0) {
        this.rect = rect;
        this.nodeIndex = index;
        this.depth = depth;
    }
}
