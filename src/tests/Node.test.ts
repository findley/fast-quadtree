import { FreeList } from '../FreeList';
import { Point } from '../shapes/Point';
import { Element } from '../Element';
import { Node } from '../Node';

describe('Node', () => {
    describe('enumerateElements()', () => {
        it('should correctly enumerate linked elements', () => {
            const list = new FreeList<Element<string>>();
            const e3 = new Element<string>('three', new Point());
            e3._next = 2;
            const e1 = new Element<string>('one', new Point());
            e1._next = 3;
            const e4 = new Element<string>('four', new Point());
            e4._next = -1;
            const e2 = new Element<string>('two', new Point());
            e2._next = 0;

            list.insert(e3);
            list.insert(e1);
            list.insert(e4);
            list.insert(e2);

            const node = new Node(4);
            node.firstChild = 1;

            const children = [];
            for (let element of node.enumerateElements(list)) {
                children.push(element);
            }

            expect(children.length).toBe(4);
            expect(children[0]).toBe(e1);
            expect(children[1]).toBe(e2);
            expect(children[2]).toBe(e3);
            expect(children[3]).toBe(e4);
        })
    });
});