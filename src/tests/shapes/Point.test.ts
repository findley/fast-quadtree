import { Point } from "../../shapes/Point";
import { Box } from "../../shapes/Box";

describe('Point', () => {
    describe('constructor', () => {
        it('should default to 0, 0', () => {
            const p = new Point();
            expect(p.x).toBe(0);
            expect(p.y).toBe(0);
        });

        it('should construct with given values', () => {
            const p = new Point(2, 3);
            expect(p.x).toBe(2);
            expect(p.y).toBe(3);
        });
    });

    describe('intersectPoint()', () => {
        it('should return true when x and y match', () => {
            const p1 = new Point(5, 6);
            const p2 = new Point(5, 6);

            const result = p1.intersectPoint(p2);

            expect(result).toBeTruthy();
        });

        it('should return false when x and y do not match', () => {
            const p1 = new Point(5, 6);
            const p2 = new Point(5, 5);

            const result = p1.intersectPoint(p2);

            expect(result).toBeFalsy();
        });
    });

    describe('intersectBox()', () => {
        it('should return true when point is inside box', () => {
            const p = new Point(25, 30);
            const box = new Box(20, 20, 20, 20);

            const result = p.intersectBox(box);

            expect(result).toBeTruthy();
        });

        it('should return false when point is outside box', () => {
            const p = new Point(1, 1);
            const box = new Box(20, 20, 20, 20);

            const result = p.intersectBox(box);

            expect(result).toBeFalsy();
        });
    });
});