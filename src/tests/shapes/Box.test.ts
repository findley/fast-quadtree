import { Box } from "../../shapes/Box";
import { SubQuad } from "../../SubQuad";
import { Point } from "../../shapes/Point";

describe('Box', () => {
    describe('constructor', () => {
        it('should construct with given values', () => {
            const b = new Box(1, 2, 3, 4);
            expect(b.x).toBe(1);
            expect(b.y).toBe(2);
            expect(b.w).toBe(3);
            expect(b.h).toBe(4);
        });
    });

    describe('center()', () => {
        it('should give Point at center of Box', () => {
            const b = new Box(0, 0, 10, 10);
            const center = b.center();
            expect(center.x).toBe(5);
            expect(center.y).toBe(5);
        });
    });

    describe('intersectPoint()', () => {
        it('should return true when Point is inside Box', () => {
            const b = new Box(0, 0, 10, 10);
            const p = new Point(1, 1);
            expect(b.intersectPoint(p)).toBeTruthy();
        });

        it('should return false when Point is outside Box', () => {
            const b = new Box(0, 0, 10, 10);
            const p = new Point(11, 11);
            expect(b.intersectPoint(p)).toBeFalsy();
        });
    });

    describe('intersectBox()', () => {
        it('should return false when box is above', () => {
            const box = new Box(20, 20, 20, 20);
            const testBox = new Box(20, 0, 20, 10);

            const result = box.intersectBox(testBox);

            expect(result).toBeFalsy();
        });

        it('should return false when box is below', () => {
            const box = new Box(20, 20, 20, 20);
            const testBox = new Box(20, 50, 20, 10);

            const result = box.intersectBox(testBox);

            expect(result).toBeFalsy();
        });

        it('should return false when box is left', () => {
            const box = new Box(20, 20, 20, 20);
            const testBox = new Box(0, 20, 10, 20);

            const result = box.intersectBox(testBox);

            expect(result).toBeFalsy();
        });

        it('should return false when box is right', () => {
            const box = new Box(20, 20, 20, 20);
            const testBox = new Box(50, 20, 10, 20);

            const result = box.intersectBox(testBox);

            expect(result).toBeFalsy();
        });

        it('should return true for exact overlap', () => {
            const box = new Box(20, 20, 20, 20);
            const testBox = new Box(20, 20, 20, 20);

            const result = box.intersectBox(testBox);

            expect(result).toBeTruthy();
        });

        it('should return false for shared edge', () => {
            const box = new Box(0, 0, 20, 20);
            const testBox = new Box(20, 0, 20, 20);

            const result = box.intersectBox(testBox);

            expect(result).toBeFalsy();
        });
    });

    describe('getSubQuad()', () => {
        it('should be TopLeft', () => {
            const box = new Box(0, 0, 100, 100);

            const quad = box.getSubQuad(10, 10);

            expect(quad).toBe(SubQuad.TopLeft);
        });

        it('should be TopRight', () => {
            const box = new Box(0, 0, 100, 100);

            const quad = box.getSubQuad(75, 10);

            expect(quad).toBe(SubQuad.TopRight);
        });

        it('should be BottomLeft', () => {
            const box = new Box(0, 0, 100, 100);

            const quad = box.getSubQuad(10, 75);

            expect(quad).toBe(SubQuad.BottomLeft);
        });

        it('should be BottomRight', () => {
            const box = new Box(0, 0, 100, 100);

            const quad = box.getSubQuad(75, 75);

            expect(quad).toBe(SubQuad.BottomRight);
        });
    });

    describe('getSubBox()', () => {
        it('should give TopLeft rect', () => {
            const box = new Box(0, 0, 100, 100);

            const rect = box.getSubBox(SubQuad.TopLeft);

            expect(rect.x).toBe(0);
            expect(rect.y).toBe(0);
            expect(rect.w).toBe(50);
            expect(rect.h).toBe(50);
        });

        it('should give TopRight rect', () => {
            const box = new Box(0, 0, 100, 100);

            const rect = box.getSubBox(SubQuad.TopRight);

            expect(rect.x).toBe(50);
            expect(rect.y).toBe(0);
            expect(rect.w).toBe(50);
            expect(rect.h).toBe(50);
        });

        it('should give BottomLeft rect', () => {
            const box = new Box(0, 0, 100, 100);

            const rect = box.getSubBox(SubQuad.BottomLeft);

            expect(rect.x).toBe(0);
            expect(rect.y).toBe(50);
            expect(rect.w).toBe(50);
            expect(rect.h).toBe(50);
        });

        it('should give BottomRight rect', () => {
            const box = new Box(0, 0, 100, 100);

            const rect = box.getSubBox(SubQuad.BottomRight);

            expect(rect.x).toBe(50);
            expect(rect.y).toBe(50);
            expect(rect.w).toBe(50);
            expect(rect.h).toBe(50);
        });
    });
});