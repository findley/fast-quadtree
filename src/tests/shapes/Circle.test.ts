import { Circle } from '../../shapes/Circle';
import { Point } from '../../shapes/Point';
import { Box } from '../../shapes/Box';
describe('Circle', () => {
    describe('intersectPoint()', () => {
        it('should return false when point is outside circle', () => {
            const c = new Circle(100, 100, 20);
            const p = new Point(10, 10);

            const result = c.intersectPoint(p);

            expect(result).toBeFalsy();
        });

        it('should return true when point is inside circle', () => {
            const c = new Circle(100, 100, 20);
            const p = new Point(90, 110);

            const result = c.intersectPoint(p);

            expect(result).toBeTruthy();
        });
    });

    describe('intersectBox()', () => {
        it('should return true when center of circle is in box', () => {
            const c = new Circle(100, 100, 20);
            const box = new Box(80, 80, 40, 40);
            
            const result = c.intersectBox(box);

            expect(result).toBeTruthy();
        });

        it('should return true when circle clips corner of box', () => {
            const c = new Circle(0, 0, 20);
            const box = new Box(13, 13, 20, 20);
            
            const result = c.intersectBox(box);

            expect(result).toBeTruthy();
        });

        it('should return true when circle clips side of box', () => {
            const c = new Circle(0, 0, 20);
            const box = new Box(19, -20, 40, 40);
            
            const result = c.intersectBox(box);

            expect(result).toBeTruthy();
        });

        it('should return false box is below circle', () => {
            const c = new Circle(0, 0, 20);
            const box = new Box(100, 100, 40, 40);
            
            const result = c.intersectBox(box);

            expect(result).toBeFalsy();
        });

        it('should return false box is above circle', () => {
            const c = new Circle(0, 0, 20);
            const box = new Box(-100, -100, 40, 40);
            
            const result = c.intersectBox(box);

            expect(result).toBeFalsy();
        });
    });
});
