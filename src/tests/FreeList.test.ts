import { FreeList } from "../FreeList";

describe('FreeList', () => {
    it('should construct correctly', () => {
        const list = new FreeList<number>();
        expect(list.size()).toBe(0);
    });

    it('should insert one and get correctly', () => {
        const list = new FreeList<number>();
        const index = list.insert(20);
        const result = list.get(index);
        expect(result).toBe(20);
    });

    it('should reuse erased element space', () => {
        const list = new FreeList<number>();
        const i1 = list.insert(10);
        list.insert(20);
        list.erase(i1);
        list.insert(30);
        expect(list.size()).toBe(2);
    });

    it('should clear correctly', () => {
        const list = new FreeList<number>();
        list.insert(10);
        list.insert(20);
        list.clear();
        expect(list.size()).toBe(0);
    });
});