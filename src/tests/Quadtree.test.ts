import { Quadtree } from '../Quadtree';
import { Point } from '../shapes/Point';
import { Box } from '../shapes/Box';

describe('Quadtree', () => {
    describe('insert()', () => {
        it('should insert into empty tree without failing', () => {
            const qt = new Quadtree<string>(new Box(0, 0, 100, 100));

            qt.insert('hello', new Point(5, 5));

            expect(qt['nodes'].length).toBe(1);
            expect(qt['elements'].size()).toBe(1);
        });

        it('should split tree when treshold is reached', () => {
            const qt = new Quadtree<string>(new Box(0, 0, 100, 100), { threshold: 1 })

            qt.insert('p1', new Point(5, 5));
            qt.insert('p2', new Point(6, 6));

            expect(qt['nodes'].length).toBe(5);
            expect(qt['elements'].size()).toBe(2);
        });

        it('should split one element to each quad', () => {
            const qt = new Quadtree<string>(new Box(0, 0, 100, 100), { threshold: 3 })

            qt.insert('p1', new Point(15, 15));
            qt.insert('p2', new Point(75, 15));
            qt.insert('p3', new Point(15, 75));
            qt.insert('p4', new Point(75, 75));

            // 4 nodes with 1 element
            expect(qt['nodes'].filter(node => node.count === 1).length).toBe(4);
            expect(qt['elements'].size()).toBe(4);
        })

        it('should split four elements to first qud', () => {
            const qt = new Quadtree<string>(new Box(0, 0, 100, 100), { threshold: 3 })

            qt.insert('p1', new Point(15, 15));
            qt.insert('p2', new Point(16, 16));
            qt.insert('p3', new Point(17, 17));
            qt.insert('p4', new Point(18, 18));

            // 1 nodes with 4 element
            expect(qt['nodes'].filter(node => node.count === 4).length).toBe(1);
            // 1 non-leaf
            expect(qt['nodes'].filter(node => node.count === -1).length).toBe(1);
            // 3 nodes with 0 elements
            expect(qt['nodes'].filter(node => node.count === 0).length).toBe(3);
            expect(qt['elements'].size()).toBe(4);
        })
    });

    describe('findInShape()', () => {
        it('should find 0 elements when out of range', () => {
            const qt = new Quadtree<string>(new Box(0, 0, 100, 100), { threshold: 2 })
            qt.insert('p1', new Point(15, 15));
            qt.insert('p2', new Point(16, 16));
            qt.insert('p3', new Point(75, 15));
            qt.insert('p4', new Point(90, 90));
            
            const results = qt.findInShape(new Box(100, 100, 100, 100));

            expect(results.length).toBe(0);
        });

        it('should find all elements when box encompasses quadtree', () => {
            const qt = new Quadtree<string>(new Box(0, 0, 100, 100), { threshold: 2 })
            qt.insert('p1', new Point(15, 15));
            qt.insert('p2', new Point(16, 16));
            qt.insert('p3', new Point(75, 15));
            qt.insert('p4', new Point(90, 90));
            
            const results = qt.findInShape(new Box(0, 0, 100, 100));

            expect(results.length).toBe(4);           
        });

        it('should find 2 elements across nodes', () => {
            const qt = new Quadtree<string>(new Box(0, 0, 100, 100), { threshold: 2 })
            qt.insert('p1', new Point(10, 10));
            qt.insert('p2', new Point(15, 15));
            qt.insert('p3', new Point(20, 15));
            qt.insert('p4', new Point(26, 15));
            qt.insert('p5', new Point(90, 90));
            
            const results = qt.findInShape(new Box(17, 10, 10, 10));

            expect(results.length).toBe(2);           
            expect(results.find(item => item.data === 'p3')).toBeDefined();
            expect(results.find(item => item.data === 'p4')).toBeDefined();
        });
    });

    describe('remove()', () => {
        it('should remove only element from quadtree', () => {
            const qt = new Quadtree<string>(new Box(0, 0, 100, 100), { threshold: 2 })
            qt.insert('p1', new Point(10, 10));

            qt.remove('p1', 10, 10);

            expect(qt.length).toBe(0);
            expect(qt.findInShape(new Box(0, 0, 100, 100)).length).toBe(0);
        });

        it('should remove last element from quadtree leaf', () => {
            const qt = new Quadtree<string>(new Box(0, 0, 100, 100), { threshold: 4 })
            qt.insert('p1', new Point(15, 15));
            qt.insert('p2', new Point(16, 16));
            qt.insert('p3', new Point(17, 17));
            qt.insert('p4', new Point(18, 18));

            qt.remove('p4', 18, 18);

            expect(qt.length).toBe(3);
            expect(qt.findInShape(new Box(0, 0, 100, 100)).length).toBe(3);
        });

        it('should remove first element from quadtree leaf', () => {
            const qt = new Quadtree<string>(new Box(0, 0, 100, 100), { threshold: 4 })
            qt.insert('p1', new Point(15, 15));
            qt.insert('p2', new Point(16, 16));
            qt.insert('p3', new Point(17, 17));
            qt.insert('p4', new Point(18, 18));

            qt.remove('p1', 15, 15);

            expect(qt.length).toBe(3);
            expect(qt.findInShape(new Box(0, 0, 100, 100)).length).toBe(3);
        });

        it('should remove middle element from quadtree leaf', () => {
            const qt = new Quadtree<string>(new Box(0, 0, 100, 100), { threshold: 4 })
            qt.insert('p1', new Point(15, 15));
            qt.insert('p2', new Point(16, 16));
            qt.insert('p3', new Point(17, 17));
            qt.insert('p4', new Point(18, 18));

            qt.remove('p3', 17, 17);

            expect(qt.length).toBe(3);
            expect(qt.findInShape(new Box(0, 0, 100, 100)).length).toBe(3);
        });
    });
});
