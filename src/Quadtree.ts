import { Point } from './shapes/Point';
import { Node } from './Node';
import { Element } from './Element';
import { FreeList } from './FreeList';
import { MaterialNode } from './MaterialNode';
import { Box } from './shapes/Box';
import { Shape } from './shapes/Shape';

export interface QuadtreeOptions {
    threshold?: number;
    maxDepth?: number;
}

const defaultOptions: Required<QuadtreeOptions> = {
    threshold: 8,
    maxDepth: 8,
};

export class Quadtree<T> {
    private elements: FreeList<Element<T>>;
    private nodes: Array<Node>;
    private rect: Box;
    private _length: number = 0;
    private freeNode: number = -1;
    private options: Required<QuadtreeOptions>;

    public constructor(rect: Box, options: QuadtreeOptions = defaultOptions) {
        this.rect = rect;
        this.options = {
            ...defaultOptions,
            ...options,
        };
        this.elements = new FreeList<Element<T>>();
        this.nodes = [new Node(0)];
    }

    public insert(data: T, point: Point) {
        const root = new MaterialNode(this.rect);
        let leaf = this.selectLeaf(root, point.x, point.y);

        if (this.shouldSplit(leaf)) {
            this.split(leaf);
            leaf = this.selectLeaf(leaf, point.x, point.y) as MaterialNode;
        }

        // Add new element
        const ele = new Element<T>(data, point);
        const index = this.elements.insert(ele);

        // Update node and next reference
        const node = this.nodes[leaf.nodeIndex];
        node.count++;
        ele._next = node.firstChild;
        node.firstChild = index;
        this._length++;
    }

    public remove(item: T, x: number, y: number): void {
        const root = new MaterialNode(this.rect);
        let leaf = this.selectLeaf(root, x, y);
        const node = this.nodes[leaf.nodeIndex];
        
        let previousElement;
        for (let index of node.enumerateIndexes(this.elements)) {
            const element = this.elements.get(index);
            if (element.data === item) {
                if (previousElement) {
                    previousElement._next = element._next;
                } else {
                    node.firstChild = element._next;
                }
                this.elements.erase(index);
                node.count--;
                this._length--;
                break;
            }
            previousElement = element;
        }
    }

    public findInShape(shape: Shape): Array<Element<T>> {
        const root = new MaterialNode(this.rect);
        const toProcess: Array<MaterialNode> = [root];
        const results = [];

        while(toProcess.length > 0) {
            const materialNode = toProcess.pop() as MaterialNode;
            const node = this.nodes[materialNode.nodeIndex];

            if (node.isLeaf()) {
                for (let element of node.enumerateElements(this.elements)) {
                    if (shape.intersectPoint(element.position)) {
                        results.push(element);
                    }
                }
            } else {
                for (let quad = 0; quad <= 3; quad++) {
                    const subBox = materialNode.rect.getSubBox(quad);
                    if (shape.intersectBox(subBox)) {
                        toProcess.push(new MaterialNode(subBox, node.firstChild + quad, materialNode.depth + 1));
                    }
                }
            }
        }

        return results;
    }

    public get length(): number {
        return this._length;
    }

    private split(materialNode: MaterialNode) {
        const node = this.nodes[materialNode.nodeIndex];
        const newNodes = [new Node(0), new Node(0), new Node(0), new Node(0)];

        let elementIndex = node.firstChild;
        for (let i = 0; i < node.count; i++) {
            const element = this.elements.get(elementIndex);
            const nextIndex = element._next;
            const subIndex = materialNode.rect.getSubQuad(element.position.x, element.position.y);
            const targetNode = newNodes[subIndex];

            if (targetNode.count > 0) {
                element._next = targetNode.firstChild;
            }
            targetNode.count += 1;
            targetNode.firstChild = elementIndex;

            elementIndex = nextIndex;
        }

        // Convert this node to be non-leaf
        node.firstChild = this.nodes.length;
        node.count = -1;
        this.nodes.push(...newNodes);
    }

    private shouldSplit(materialNode: MaterialNode): boolean {
        const node = this.nodes[materialNode.nodeIndex];
        return node.count + 1 > this.options.threshold && materialNode.depth < this.options.maxDepth;
    }

    private selectLeaf(root: MaterialNode, sx: number, sy: number): MaterialNode {
        const toProcess: Array<MaterialNode> = [root];

        while(toProcess.length > 0) {
            const materialNode = toProcess.pop() as MaterialNode;
            const node = this.nodes[materialNode.nodeIndex];

            if (node.isLeaf()) {
                return materialNode;
            } else {
                const quad = materialNode.rect.getSubQuad(sx, sy);
                const rect = materialNode.rect.getSubBox(quad);
                toProcess.push(new MaterialNode(rect, node.firstChild + quad, materialNode.depth + 1));
            }
        }
        
        throw "Invalid quadtree";
    }
}