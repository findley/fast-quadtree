import { Point } from './shapes/Point';

export class Element<T> {
    data: T;

    position: Point;

    // Points to the next element in the leaf node. A value of -1 indicates
    // the end of the list.
    _next: number;

    public constructor(data: T, point: Point) {
        this.data = data;
        this.position = point;
    }
}
