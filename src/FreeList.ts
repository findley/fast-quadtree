class FreeElement<T> {
    public element: T;
    public next: number = -1;
}

export class FreeList<T> {
    private data: Array<FreeElement<T>>;
    private firstFree: number;

    constructor() {
        this.data = [];
        this.firstFree = -1;
    }

    public get(n: number): T {
        return this.data[n].element;
    }

    public insert(element: T): number {
        if (this.firstFree !== -1) {
            const index = this.firstFree;
            this.firstFree = this.data[this.firstFree].next;
            this.data[index].element = element;

            return index;
        } else {
            const fe = new FreeElement<T>();
            fe.element = element;
            this.data.push(fe);

            return this.data.length - 1;
        }
    }

    public erase(n: number): void {
        this.data[n].next = this.firstFree;
        this.firstFree = n;
    }

    public clear(): void {
        this.data.length = 0;
        this.firstFree = -1;
    }

    public size(): number {
        return this.data.length;
    }
}