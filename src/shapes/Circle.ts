import { Shape } from './Shape';
import { Point } from './Point';
import { Box } from './Box';

export class Circle implements Shape {
    public x: number;
    public y: number;
    public r: number;

    public constructor(x: number, y: number, radius: number) {
        this.x = x;
        this.y = y;
        this.r = radius;
    }

    public intersectPoint(p: Point): boolean {
        const dx = p.x-this.x;
        const dy = p.y-this.y;

        return dx * dx + dy * dy < this.r * this.r;
    }

    public intersectBox(box: Box): boolean {
        const nearestX = this.clamp(this.x, box.x, box.x + box.w);
        const nearestY = this.clamp(this.y, box.y, box.y + box.h);

        const dx = this.x - nearestX;
        const dy = this.y - nearestY;

        return dx * dx + dy * dy < this.r * this.r;
    }

    private clamp(val: number, min: number, max: number): number {
        if (val < min) return min;
        if (val > max) return max;
        return val;
    }
}
