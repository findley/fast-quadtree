import { Shape } from './Shape';
import { Box } from './Box';

export class Point implements Shape {
    public x: number;
    public y: number;

    public constructor(x: number = 0, y: number = 0) {
        this.x = x;
        this.y = y;
    }

    public intersectBox(box: Box): boolean {
        return box.intersectPoint(this);
    }

    public intersectPoint(point: Point): boolean {
        return point.x === this.x && point.y === this.y;
    }
}