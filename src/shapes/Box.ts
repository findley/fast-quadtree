import { Point } from './Point';
import { Shape } from './Shape';
import { SubQuad } from '../SubQuad';

export class Box implements Shape {
    public x: number;
    public y: number;
    public w: number;
    public h: number;

    public constructor(x: number, y: number, width: number, height: number) {
        this.x = x;
        this.y = y;
        this.w = width;
        this.h = height;
    }

    public center(): Point {
        return new Point(this.x + (this.w >> 1), this.y + (this.h >> 1));
    }

    public intersectPoint(p: Point): boolean {
        return (
            this.x <= p.x && p.x <= this.x + this.w &&
            this.y <= p.y && p.y <= this.y + this.h
        );
    }

    public intersectBox(box: Box): boolean {
        if (box.x + box.w <= this.x || this.x + this.w <= box.x) {
            return false;
        }

        if (box.y + box.h <= this.y || this.y + this.h <= box.y) {
            return false;
        }
        
        return true;
    }

    public getSubQuad(x: number, y: number): SubQuad {
        const center = this.center();

        if (x < center.x) {
            if (y < center.y) {
                return SubQuad.TopLeft;
            }
            return SubQuad.BottomLeft;
        }
        if (y < center.y) {
            return SubQuad.TopRight;
        }
        return SubQuad.BottomRight;
    }

    public getSubBox(quad: SubQuad): Box {
        const halfWidth = this.w >> 1;
        const halfHeight = this.h >> 1;

        switch(quad) {
            case SubQuad.TopLeft:
                return new Box(this.x, this.y, halfWidth, halfHeight);
            case SubQuad.TopRight:
                return new Box(this.x + halfWidth, this.y, halfWidth, halfHeight);
            case SubQuad.BottomLeft:
                return new Box(this.x, this.y + halfHeight, halfWidth, halfHeight);
            case SubQuad.BottomRight:
            default:
                return new Box(this.x + halfWidth, this.y + halfHeight, halfWidth, halfHeight);
        }
    }
}
