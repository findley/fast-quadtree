import { Box } from './Box';
import { Point } from './Point';

export interface Shape {
    intersectBox(box: Box): boolean;
    intersectPoint(point: Point): boolean;
}
