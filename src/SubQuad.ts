export enum SubQuad {
    TopLeft = 0,
    TopRight,
    BottomLeft,
    BottomRight,
}