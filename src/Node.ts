import { FreeList } from './FreeList';
import { Element } from './Element';

export class Node {
    // Points to the first child if this node is a branch or the first element
    // if this node is a leaf.
    public firstChild: number = -1;

    // Stores the number of elements in the leaf or -1 if this node
    // is not a leaf.
    public count: number = -1;

    public constructor(count: number) {
        this.count = count;
    }

    public isLeaf(): boolean {
        return this.count >= 0;
    }

    public *enumerateIndexes<T>(elements: FreeList<Element<T>>): IterableIterator<number> {
        let index = this.firstChild;

        for (let i = 0; i < this.count; i++) {
            yield index;
            index = elements.get(index)._next;
        }
    }

    public *enumerateElements<T>(elements: FreeList<Element<T>>): IterableIterator<Element<T>> {
        for (let index of this.enumerateIndexes(elements)) {
            yield elements.get(index);
        }
    }
}
