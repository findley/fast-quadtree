module.exports = {
    rootDir: 'src/',
    collectCoverageFrom: [
        "**/*.ts",
        "!index.ts",
        "!tests/**"
    ],
    transform: {
        "^.+\\.ts$": "ts-jest"
    }
};
