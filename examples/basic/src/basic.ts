import { Quadtree, Box, Point, Circle } from 'fast-quadtree';

// Define object / type we want to store in the quadtree
interface Item {
    x: number;
    y: number;
    label: string;
}

// Create some items in a 100 by 100 area using sin()
const items: Array<Item> = [];
for (let x = 0; x < 100; x += 2) {
    const y = (Math.sin(x / 100 * Math.PI * 2) + 1) * (100 / 2);
    items.push({x, y, label: `Item ${x}`});
};

// Create a new quadtree of Items that is 100 by 100
const qt = new Quadtree<Item>(new Box(0, 0, 100, 100));

// Insert each of the items to our quadtree
for (let item of items) {
    qt.insert(item, new Point(item.x, item.y));
}

// Query the quadtree and log the results
const result = qt.findInShape(new Circle(50, 50, 20));
result.forEach(element => {
    console.log(element.data);
});
