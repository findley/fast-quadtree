# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [UNRELEASED]
### Added
- Added `remove()` method for removing an element that has matching data and position. ([@findley](https://gitlab.com/findley) in [!21](https://gitlab.com/findley/fast-quadtree/merge_requests/21))
- Added a basic example in `/examples/basic`. ([@findley](https://gitlab.com/findley) in [!20](https://gitlab.com/findley/fast-quadtree/merge_requests/20))
- Added usage documentation to README.md. ([@findley](https://gitlab.com/findley) in [!20](https://gitlab.com/findley/fast-quadtree/merge_requests/20))

### Changed

### Deprecated

### Removed

### Fixed

### Security

## [0.6.0] - 2019-06-16
### Fixed
- Fixed more CI issues that were preventing typescript declarations from being published. ([@findley](https://gitlab.com/findley) in [!17](https://gitlab.com/findley/fast-quadtree/merge_requests/17))

## [0.5.0] - 2019-06-16 - 💀 (broken)
### Fixed
- Fixed an issue with gitlab-ci publish that was preventing build output from being included in the published tarball. ([@findley](https://gitlab.com/findley) in [!14](https://gitlab.com/findley/fast-quadtree/merge_requests/14))

## [0.4.0] - 2019-06-16 - 💀 (broken)
### Added
- Added `findInShape()` method to query quadtree. ([@findley](https://gitlab.com/findley) in [!8](https://gitlab.com/findley/fast-quadtree/merge_requests/8))
- Added `Shape` interface and `Circle` to facilitate quadtree operations. ([@findley](https://gitlab.com/findley) in [!8](https://gitlab.com/findley/fast-quadtree/merge_requests/8))

### Changed
- gitlab-ci build and test steps only run on dev, feature/* and release/*. ([@findley](https://gitlab.com/findley) in [!9](https://gitlab.com/findley/fast-quadtree/merge_requests/9))

### Fixed
- npm prerelease command now does npm install. ([@findley](https://gitlab.com/findley) in [!9](https://gitlab.com/findley/fast-quadtree/merge_requests/9))
- Made README links non-relative. ([@findley](https://gitlab.com/findley) in [!10](https://gitlab.com/findley/fast-quadtree/merge_requests/10))

## [0.3.0] - 2019-06-10 - 💀 (broken)
### Added
- Quadtree implementation with `insert()` capability. ([@findley](https://gitlab.com/findley) in [!3](https://gitlab.com/findley/fast-quadtree/merge_requests/3))